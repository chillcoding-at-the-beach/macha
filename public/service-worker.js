var cacheName = 'cvPWA-v-2-1';
var filesToCache = [
  './',
  './index.html',
  './profile.html',
  './agenda.html',
  './contact.html',
  './manifest.json',
  './scripts/app.js',
  './scripts/materialize/materialize.min.js',
  './scripts/jquery-3.2.1.min.js',
  './styles/materialize/materialize.min.css',
  './styles/font-awesome/fonts/fontawesome-webfont.woff2',
  './styles/font-awesome/css/font-awesome.min.css',
  './images/favicon.ico',
  './images/icons/icon-152x152.png',
  './images/icons/icon-192x192.png',
  './images/icons/icon-256x256.png',
  './images/ic_refresh_white_24px.svg',
  './images/macha.png'
];

self.addEventListener('install', function(e) {
  console.log('[ServiceWorker] Install');
  e.waitUntil(
    caches.open(cacheName).then(function(cache) {
      console.log('[ServiceWorker] Caching app shell');
      return cache.addAll(filesToCache);
    })
  );
});

self.addEventListener('beforeinstallprompt', function(e) {
  // For more details read: https://developers.google.com/web/fundamentals/getting-started/primers/promises
  e.userChoice.then(function(choiceResult) {

    console.log(choiceResult.outcome);

    if(choiceResult.outcome == 'dismissed') {
      console.log('User cancelled home screen install');
    }
    else {
      console.log('User added to home screen');
    }
  });
});

self.addEventListener('activate', function(e) {
  console.log('[ServiceWorker] Activate');
  e.waitUntil(
    caches.keys().then(function(keyList) {
      return Promise.all(keyList.map(function(key) {
        if (key !== cacheName) {
          console.log('[ServiceWorker] Removing old cache', key);
          return caches.delete(key);
        }
      }));
    })
  );
  return self.clients.claim();
});

self.addEventListener('fetch', function(e) {
  console.log('[ServiceWorker] Fetch', e.request.url);
  e.respondWith(
    caches.match(e.request).then(function(response) {
      return response || fetch(e.request);
    })
  );
});
