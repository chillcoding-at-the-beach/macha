macha is a CV project for the **Mobiles Technologies** course that occurs 
in the french riviera (at Sophia Antipolis and Aix).

34 students attend this course in 2017.


This courses will start with the trend in mobile (statistics in world about mobile user and mobile application), 
followed by the constraint in mobile development (cost, benefit & time), 
and finish on development solutions.

The aim of this courses is to use as much as possible tools that are seen 
in other courses (IHM, project management, english, communication, 
native development, data base, etc.).

## Examples of Mobile Web Application
+ [CV Project](https://gitlab.com/chillcoding-at-the-beach/macha/-/wikis/home)
+ [Pamppa Bocca](https://gitlab.com/chillcoding-at-the-beach/pampa-bocca)

The common thread of all the seances is a cv project.

## Course Plan

-> [Calendrier](https://gitlab.com/chillcoding-at-the-beach/macha/-/wikis/Calendrier)

* Trends in mobile, constraints and solutions in mobile development
* Design (Trends : MD / iOS, Material Design)
* Cross platform solution (Native / Web Application)  
* Web Site as a prototype (SSG, Static Site Generator)
* Progressive Web App (PWA) Configuration
* DB Communication SERVER / CLIENT
* Technologies choice
* Zoom on IONIC / XAMARIN / UNITY / KOTLIN Lib.
* Project Achievement
* Cross Platform Architecture
* Final Presentation
